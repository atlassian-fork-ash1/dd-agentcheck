#! /bin/bash

set -eux

micros_env="${1}" # ddev or prod-east
micros_name="dd-agentcheck"
docker_name="docker.atlassian.io/atlassian/${micros_name}:latest"

docker build -t ${docker_name} ./app
docker push ${docker_name}

# micros stash:set API_KEY <Datadog API key> -e ${micros_env} -s ${micros_name}
micros service:deploy ${micros_name} -e ${micros_env}
