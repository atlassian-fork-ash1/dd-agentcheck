#! /bin/bash

curl localhost:8080/targets -d '{"url": "http://mock:5000"}'
curl localhost:8080/targets -d '{"url": "http://mock:5000/full"}'
curl localhost:8080/targets

# curl -X DELETE localhost:8080/targets -d '{"url": "http://mock:5000"}'


# curl 'https://dd-agentcheck.internal.domain.dev.atlassian.io/targets' -d '{"url": "https://mock.ngrok.io"}'
# curl 'https://dd-agentcheck.internal.domain.dev.atlassian.io/targets' -d '{"url": "https://mock.ngrok.io/full"}'
# curl 'https://dd-agentcheck.internal.domain.dev.atlassian.io/targets'

# curl 'https://dd-agentcheck.atlassian.io/targets' -d '{"url": "https://mock.ngrok.io"}'
# curl 'https://dd-agentcheck.atlassian.io/targets' -d '{"url": "https://mock.ngrok.io/full"}'
# curl 'https://dd-agentcheck.atlassian.io/targets'
