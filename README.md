# Datadog agent check service

Micros service allowing you to register a URL to be checked for metrics in a generic JSON format
that will be posted to Datadog.


## Example

*Step 1.* Add a metrics enpoint to you app that exposes JSON that looks like this...

    {
      "gauge": {
        "my.gauge": {
          "value": 3834,
          "tags": [
            "mytag:mytagvalue",
            "myothertag:myothertagvalue"
          ]
        }
      }
    }

*Step 2.* Register your app's metrics endpoint with this dd-agentcheck service...

    curl 'https://dd-agentcheck.atlassian.io/targets' -d '{"url": "https://myapp.atlassian.io/metrics"}'

*Step 3.* View your metrics in Datadog...

![dd-agentcheck.png](https://bitbucket.org/repo/5BxqqM/images/249514064-dd-agentcheck.png)

> Note: `host` is set automatically.


## JSON payload format

The format of the JSON payload expected as a response to GET requests on your metrics URL is
inspired by the methods on Datadog's
[AgentCheck class](https://github.com/DataDog/dd-agent/blob/master/checks/__init__.py#L293) which
serves as the base class for custom agent checks.

A quick overview of the method signatures...

    count           (metric, value=0,  tags=None, hostname=None, device_name=None)
    decrement       (metric, value=-1, tags=None, hostname=None, device_name=None)
    gauge           (metric, value,    tags=None, hostname=None, device_name=None, timestamp=None)
    histogram       (metric, value,    tags=None, hostname=None, device_name=None)
    increment       (metric, value=1,  tags=None, hostname=None, device_name=None)
    monotonic_count (metric, value=0,  tags=None, hostname=None, device_name=None)
    rate            (metric, value,    tags=None, hostname=None, device_name=None)

> Note: `gauge` will probably be the most popular method implemented by app metrics endpoints.
>       Counters and histograms (timings, etc) are generally better tracked using dogstatsd from
>       within your app, instead of polling using this agent check service. Gauges represent
>       measuring the level of something at a given point in time. An example is the row count of a
>       database table. These are a better fit for polling using a Datadog agent check.

All of these methods take the following arguments:

* metric: The name of the metric
* value: The value for the metric (defaults to 1 on increment, -1 on decrement)
* tags: (optional) A list of tags to associate with this metric.
* hostname: (optional) A hostname to associate with this metric. Defaults to the current host.
* device_name: (optional) A device name to associate with this metric.

> Note: `hostname` will be filled in automatically by the `http_json` check using the hostname from
>       the registered URL.

> Note: `device_name` will most likely not apply to the custom app metrics registered with this
>       service.

You can form any number of metrics in your JSON payload using the method definitions from above. For
another example if you want two histogram metrics and one incremented counter...

    {
      "histogram": {
        "my.histogram.metric": {
          "value": 12,
          "tags": [
            "mytag:mytagvalue",
            "myothertag:myothertagvalue"
          ]
        },
        "my.other.histogram.metric": {
          "value": 3413
        }
      },
      "increment": {
        "my.incremented.counter": {
        }
    }


## API

For external use...

  POST   /targets {'url': 'https://myapp.atlassian.io'} -> 200
  DELETE /targets {'url': 'https://myapp.atlassian.io'} -> 200


Used by the `http_json` check...

  GET   /targets                                        -> 200
  PUT   /targets  {'url': 'https://myapp.atlassian.io'} -> 200, 409

> Note: PUT updates the timestamp_utc field in DynamoDB to ensure that metric endpoints are hit no
>       more than once every 15 seconds. It responds with a 409 status code (`conflict`) if the
>       endpoint has been collected within the past 15 seconds.


## Reference

* [Writing an Agent Check](http://docs.datadoghq.com/guides/agent_checks/)
* [AgentCheck class](https://github.com/DataDog/dd-agent/blob/master/checks/__init__.py#L293)
