#! /usr/bin/env python3.4

"""Flask app that exposes API for registering targets for the http_json check.

Listens for POSTs to /targets that contains a simple JSON payload of 

    {"url": "https://myapp.atlassian.io/metrics"}

Where the url value points to an endpoint on your app that expects a GET
request and responds with JSON in the form

    {
        "gauge": {
            "my.gauge": {
                "value": 3834,
                "tags": [
                    "mytag:mytagvalue",
                    "myothertag:myothertagvalue"
                ]
            }
        }
    }
"""

from calendar import timegm
from decimal import Decimal
from os import environ
from json import JSONEncoder
from time import gmtime
from urllib.parse import urlparse

from boto3 import resource
from botocore.exceptions import ClientError
from gevent.wsgi import WSGIServer
from flask import Flask, jsonify, make_response, request

IN_LOCAL_DEV = 'DYNAMO_LOCAL_DEV_URL' in environ

class DecimalEncoder(JSONEncoder):
    def default(self, o):
        if not isinstance(o, Decimal):
            return super().default(o)
        return float(o) if o % 1 > 0 else int(o)

def connect_to_dynamodb():
    if IN_LOCAL_DEV:
        kwargs = {'endpoint_url': environ['DYNAMO_LOCAL_DEV_URL']}
    else:
        kwargs = {'region_name': environ['DYNAMO_TARGETS_TABLE_REGION']}
    return resource('dynamodb', **kwargs)

def create_targets_table():
    if not IN_LOCAL_DEV:
        return
    dynamodb = connect_to_dynamodb()
    try:
        table = dynamodb.create_table(
            TableName=environ['DYNAMO_TARGETS_TABLE_NAME'],
            KeySchema=[{'AttributeName': 'url', 'KeyType': 'HASH'}],
            AttributeDefinitions=[{'AttributeName': 'url', 'AttributeType': 'S'}],
            ProvisionedThroughput={'ReadCapacityUnits': 100, 'WriteCapacityUnits': 100})
    except ClientError as e:
        print('{!r}'.format(e))
        return
    table.wait_until_exists()

def do_on_targets_table(action, **kwargs):
    dynamodb = connect_to_dynamodb()
    table = dynamodb.Table(environ['DYNAMO_TARGETS_TABLE_NAME'])
    act = getattr(table, action)
    return act(**kwargs)


app = Flask(__name__)
app.json_encoder = DecimalEncoder

@app.route('/targets', methods=['POST'])
def add_target():
    target = request.get_json(force=True)
    url = urlparse(target['url'])
    assert url.scheme and url.netloc
    existing = do_on_targets_table('get_item', Key={'url': target['url']})
    if 'Item' in existing and existing['Item']:
        return make_response(jsonify(existing['Item']), 409)
    target['timestamp_utc'] = 0
    response = do_on_targets_table('put_item', Item=target)
    return jsonify(response)

@app.route('/targets', methods=['DELETE'])
def delete_target():
    target = request.get_json(force=True)
    response = do_on_targets_table('delete_item', Key=target)
    return jsonify(response)

@app.route('/targets', methods=['GET'])
def list_targets():
    targets = []
    while True:
        response = do_on_targets_table('scan')
        for target in response['Items']:
            targets.append(target)
        if 'LastEvaluatedKey' not in response:
            break
    return jsonify(targets)

@app.route('/targets', methods=['PUT'])
def collect_metrics():
    """An instance of the `http_json` check calls this method to announce its
    intention and request permission to collect metrics for a target. Multiple
    hosts will be running the check in production micros, so it is important
    to throttle metrics collection and not hammer the apps.
    """
    target = request.get_json(force=True)
    min_collection_interval = target['min_collection_interval']
    del target['min_collection_interval']
    now = timegm(gmtime())
    try:
        do_on_targets_table('update_item',
            Key={'url': target['url']},
            UpdateExpression='set timestamp_utc = :now',
            ConditionExpression='timestamp_utc <= :last_cycle',
            ExpressionAttributeValues={
                ':now': now,
                ':last_cycle': now - min_collection_interval})
        status_msg = 'ready for metrics collection'
        status_code = 200

    except ClientError as e:
        if e.response['Error']['Code'] != "ConditionalCheckFailedException":
            raise
        status_msg = 'metrics collected within last collection cycle'
        status_code = 409

    return make_response(jsonify({'status': status_msg}), status_code)


@app.route('/healthcheck', methods=['GET'])
def healthcheck():
    return jsonify({'status': 'OK'})

if __name__ == "__main__":
    create_targets_table()
    server = WSGIServer(('', 8080), app)
    server.serve_forever()
